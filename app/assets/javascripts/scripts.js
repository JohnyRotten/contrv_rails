$(function(){
	function initDesktop() 
	{
		$('body').off();
		// Preparing
		$('.slider').find('.slide').first().addClass('current')
		$('.nav').find('li').first().addClass('current');

		// Navigation
		function changeCurrent(item) {
			var href = item.find('a').attr('href');
			if (href === undefined) return;
			$('#main_nav .nav li').removeClass('current');
			item.addClass('current');
			$('#main').css('margin-top', -1*($(href).position().top));
		}
		$('body').on('click', '#main_nav .nav a, .nav-href', function(){
			changeCurrent($(this).parent());
			return false;
		});
		$('body').on('click', '#nav_arrows .prev', function(){
			var curr = $('#main_nav .nav .current');
			var coll = $('#main_nav .nav li');
			if ( coll.first() !== curr ) {
				changeCurrent(curr.prev());
			}
			return false;
		});
		$('body').on('click', '#nav_arrows .next', function(){
			var curr = $('#main_nav .nav .current');
			var coll = $('#main_nav .nav li');
			if ( coll.first() !== curr ) {
				changeCurrent(curr.next());
			}
			return false;
		});
		$('body').on('mousewheel', '#main', function(event, delta){
			if (delta < 0) {
				$('#nav_arrows .next').trigger('click');
			} else {
				$('#nav_arrows .prev').trigger('click');
			}
			return false;
		});

		// Slider
		function changeSlide(slide) {
			slide.closest('.slider').find('.slide').removeClass('current');
			slide.addClass('current');
			slide.closest('.slide-frame').css('left', -1*slide.position().left);
		}
		$('body').on('click', '.slider .prev', function(){
			var slides = $(this).closest('.slider').find('.slide');
			var curr = slides.filter('.current');
			var prev = curr.prev();
			if (prev.length === 0) {
				prev = slides.last();
			}
			changeSlide(prev);
			return false;
		});
		$('body').on('click', '.slider .next', function(){
			var slides = $(this).closest('.slider').find('.slide');
			var curr = slides.filter('.current');
			var next = curr.next();
			if (next.length === 0) {
				next = slides.first();
			}
			changeSlide(next);
			return false;
		});

		// Video player
		$(".player").mb_YTPlayer();
	}

	function initMobile() 
	{
		// Unbinding
		$('.mbYTP_wrapper').remove();
		$('body').off();
		$('#main').css('margin-top', 0);

		$('body').on('click', '#nav_btn', function(){
			var menu = $('#main_nav ul');
			if (menu.hasClass('opened')) {
				menu.removeClass('opened');
			} else {
				menu.addClass('opened');
			}
			return false;
		});
	}

	function init() {
		if (window.matchMedia("(max-width: 720px)").matches) {
			initMobile();
		} else {
			initDesktop();
		}
	}

	init();

	$(window).resize(init);
});
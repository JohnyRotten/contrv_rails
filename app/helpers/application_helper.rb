module ApplicationHelper
	def cms?
		@cms || false
	end

	def set_cms_mode
		@cms = true
	end

	def full_title(title)
		base_title = 'ООО "Контр-В"'
		return "#{base_title} | #{title}" unless title.empty?
		return base_title
	end

	def meta_title(str = nil)
		"Охранное предприятие ООО 'Контр-В' (физическая охрана)"
	end

	def meta_desc(str = nil)
		"Охранное предприятие ООО 'Контр-В' предоставляет физическую охрану, охрану недвижимости, установку сигнализаций и сопровождение грузов."
	end

	def meta_keywords(str = nil)
		"охрана, охранное предприятие, физическая охрана, охрана предприятия, охрана квартир, сопровождение грузов, установка сигнализаций, системы сигнализаций"
	end

	def get_page(href)
		page = Page.find_by(href: href)
		return "Not found" if page.nil?
		return page.body
	end

	def get_cms_menu
		if current_user.nil?
			@menu = unuser_menu 
		else
			@menu = admin_menu
		end
	end

	def get_default_menu
		@menu = Menu.where(enabled: true).to_a.sort
	end

	def get_services
		Service.where(enabled: true).order(:order)
	end

	private

		def unuser_menu
			arr = []
			arr << Menu.new(title: 'Вход',			href: '/signin')
			arr << Menu.new(title: 'Регистрация',	href: '/signup')
			arr
		end

		def admin_menu
			arr = []
			arr << Menu.new(title: 'Пользователи',	href: '/users')
			arr << Menu.new(title: 'Страницы',		href: '/pages')
			arr << Menu.new(title: 'Меню',			href: '/menus')
			arr << Menu.new(title: 'Услуги',		href: '/services')
			arr << Menu.new(title: 'Выход',			href: '/signup')
			arr
		end
end

module SessionsHelper
	def sign_in(user)
		remember_token = User.new_remember_token
		cookies.permanent[:remember_token] = remember_token
		user.update_attribute :remember_token, User.encrypt(remember_token)
		self.current_user = user
	end

	def current_user=(user)
		@current_user = user
	end

	def current_user
		remember_token = User.encrypt(cookies[:remember_token])
		@current_user ||= User.find_by(remember_token: remember_token)
	end

	def current_user?(user)
		current_user == user
	end

	def signed_in?
		!current_user.nil?
	end

	def admin?
		signed_in? && current_user.admin?
	end

	def signed_in_user
		unless signed_in?
			store_location
			redirect_to signin_url,	notice: 'Please sign in.'
		end
	end

	def sign_out
		current_user.update_attribute :remember_token, User.encrypt(User.new_remember_token)
		cookies.delete :remember_token
		self.current_user = nil
	end

	def auth_user
		redirect_to root_url if signed_in?
	end

	def correct_user
      @user = User.find(params[:id])
      redirect_to root_url unless current_user?(@user)
    end

    def admin_user
      redirect_to root_url unless current_user.admin?
    end

	def redirect_back_or(path)
		redirect_to(session[:return_to] || path)
		session.delete :return_to
	end

	def store_location
		session[:return_to] = request.url if request.get?
	end
end

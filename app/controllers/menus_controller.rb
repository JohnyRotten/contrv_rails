class MenusController < ApplicationController
	before_action :admin_user
	before_action :set_cms_mode
	before_action :get_cms_menu

	def index
		@menus = Menu.all.order(:order)
	end

	def create
		@menu = Menu.create(menu_params)
		if @menu.save
			flash[:success] = 'Menu item was created.'
		else
			flash[:error] = "Menu item don't created."
		end	
		redirect_to menus_path
	end

	def update
		@menu = Menu.find(params[:id])
		if @menu.update_attributes menu_params
			flash[:success] = 'Menu item was updated.'
		else
			flash[:error] = "Menu item don't was updated."
		end
		redirect_to menus_path
	end

	def destroy
		Menu.find(params[:id]).destroy
		flash[:success] = "Menu item was removed"
		redirect_to menus_path
	end

	private

		def menu_params
			params.require(:menu).permit(:order, :title, :href, :enabled)
		end
end

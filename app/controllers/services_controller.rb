class ServicesController < ApplicationController
	before_action :set_cms_mode,	only: [ :index, :new, :edit ]
	before_action :get_cms_menu,	only: [ :index, :new, :edit ]
	before_action :admin_user,		only: [ :index, :new, :edit ]

	def index
		@services = Service.paginate(page: params[:page])
	end

	def new
		@service = Service.new
	end

	def create
		@service = Service.create(service_params)
		if @service.save
			flash[:success] = "Service created."
		else
			flash[:error] = "Service don't created."
		end
		redirect_to services_path
	end

	def edit
		@service = Service.find(params[:id])
	end

	def update
		@service = Service.find(params[:id])
		if @service.update_attributes service_params
			flash[:success] = "Service updated."
		else
			flash[:error] = "Service don't updated."
		end
		redirect_to services_path
	end

	def destroy
		Service.find(params[:id]).destroy
		flash[:success] = "Service was removed."
		redirect_to services_path
	end

	private

		def service_params
			params.require(:service).permit(:title, :desc, :enabled, :order)
		end
end

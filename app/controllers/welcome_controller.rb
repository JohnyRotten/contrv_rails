class WelcomeController < ApplicationController
	before_action :get_default_menu

	def home
		@pages = Page.where(enabled: true).to_a.sort#(&:order)
	end
end

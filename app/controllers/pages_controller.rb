class PagesController < ApplicationController
	before_action :set_cms_mode, 	only: [ :index, :new, :edit, :create, :update, :destroy ]
	before_action :admin_user,		only: [ :index, :new, :edit, :create, :update, :destroy ]
	before_action :get_cms_menu,	only: [ :index, :new, :edit, :create, :update, :destroy ]
	before_action :get_default_menu,	only: :show		

	def index
		@pages = Page.paginate(page: params[:page])
	end

	def new
		@page = Page.new
	end

	def create
		@page = Page.create(page_params)
		if @page.save
			flash[:success] = "Page was created."
			redirect_to @page
		else
			flash[:error] = "Page don't created."
			render 'new'
		end
	end

	def show
		@page = Page.find(params[:id])
	end

	def edit
		@page = Page.find(params[:id])
	end

	def update
		@page = Page.find(params[:id])
		if @page.update_attributes page_params
			flash[:success] = "Page updated."
			redirect_to @page
		else
			flash[:error] = "Page don't updated."
			render 'new'
		end
	end

	def destroy
		Page.find(params[:id]).destroy
		flash[:success] = "Page was removed."
		redirect_to pages_path
	end

	private

		def page_params
			params.require(:page).permit(:title, :href, :enabled, :body, 
								:meta_title, :meta_desc, :meta_keywords)
		end
end

class UsersController < ApplicationController
  before_action :set_cms_mode #, only: [ :index, :show, :new, :edit ]
  before_action :get_cms_menu
  
  before_action :signed_in_user,  only: [ :edit,    :update,  :index ]
  # before_action :correct_user,    only: [ :edit,    :update ]
  # before_action :auth_user,       only: [ :new,     :create ]
  # before_action :admin_user,      only: [ :destroy, :index ]

  def index
    @users = User.paginate(page: params[:page])
  end

  def show
    @user = User.find(params[:id])
  end

  def new
    @user = User.new
  end

  def create
    @user = User.create(user_params)
    if @user.save
      flash[:success] = 'User created'
      unless signed_in?
        sign_in @user
        redirect_to @user 
      else
        redirect_to users_path 
      end
    else
      flash.now[:error] = "User don't created"
      render 'new'
    end
  end

  def edit
    @user = User.find(params[:id])
  end

  def update
    @user = User.find(params[:id])
    if @user.update_attributes user_params
      flash[:success] = 'User updated'
      redirect_to @user
    else
      flash.now[:error] = "User don't updated"
      render 'edit'
    end
  end

  def destroy
    User.find(params[:id]).destroy
    flash[:success] = 'User was removed.'
    redirect_to users_path
  end

  private

    def user_params
      params.require(:user).permit(:name, :email, :password, :password_confirmation)
    end
end

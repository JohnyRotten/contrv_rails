User.create name: 'admin', email: 'admin@example.com', password: '0GTPjX', password_confirmation: '0GTPjX', admin: true
User.create name: 'contrv',	email: 'contr-v@mail.ru',	password: 'contr-v', password_confirmation: 'contr-v', admin: true

Menu.create title: 'Главная', href: 'top', enabled: true, order: 1
Menu.create title: 'Услуги', href: 'services', enabled: true, order: 2
Menu.create title: 'Обратная связь', href: 'contact', enabled: true, order: 3
Menu.create title: 'Отзывы', href: 'reviews', enabled: true, order: 4
Menu.create title: 'Где нас найти', href: 'map', enabled: true, order: 5
Menu.create title: 'О нас', href: 'about', enabled: true, order: 6

Page.create title: 'Главная', href: 'top', enabled: true, body: '
					<h1>Ассоциация Охранных предприятий "Контр-В"</h1>
					<h3>
						Общество с ограниченной ответственностью Охранное Предприятие "Контр-В"
					</h3>
					<p>Лицензия ЧО 009337 до 23 мая 2018 года. </p>
					<p>ИНН/4705060743</p> 
					<p>огрн /1134705001089</p>
					<p>
						188510 Ленинградская область Волосовский район<br/>
						г.Волосово Гатчинская д. 3а<br/>
						e-mail: <a href="#contact" class="nav-href">contr-v@mail.ru</a><br/>
						тел. 8(81373) 25 021<br/>
						+7 921 447 40 30
					</p>'

Page.create title: 'Услуги', href: 'services', enabled: true, body: '
					<div class="services slider">
						<span class="prev">
							<i class="fa fa-chevron-left fa-2x"></i>
						</span>
						<span class="next">
							<i class="fa fa-chevron-right fa-2x"></i>
						</span>
						<ul class="slide-frame">
							<div class="vertical-middle"></div>
							<% get_services.each do |service| %>
							<li class="slide">
								<h1><%= service.title %></h1>
								<%= render inline: service.desc %>
							</li>
							<% end %>
						</ul>
					</div>'

Page.create title: 'Обратная связь', href: 'contact', enabled: true, body: '
					<h1>Обратная связь</h1>
					<form class="form" method="post" action="/contact">
						<label for="from">От кого:</label>
						<input type="email" id="from" name="from">
						<label for="subject">Тема:</label>
						<input type="text" id="subject" name="subject">
						<label for="msg">Текст сообщения:</label>
						<textarea name="msg" id="msg" cols="30" rows="5"></textarea>
						<input type="submit" value="Оправить">
					</form>'

Page.create title: 'Отзывы', href: 'reviews', enabled: true, body: '
					<h1>Отзывы</h1>
						<ul class="reviews">
					</ul>'

Page.create title: 'Где нас найти', href: 'map', enabled: true, body: '
					<h1>Где нас найти</h1>
					<p>188410 Ленинградская область<br>г. Волосово,<br>ул. Гатчинская, д. 3а</p>
					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d4055.5175192944735!2d29.498026200286972!3d59.453773957128924!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4695e0325c28caf7%3A0x524290336ef391ff!2z0JPQsNGC0YfQuNC90YHQutCw0Y8g0YPQuy4sIDM!5e0!3m2!1sru!2sru!4v1397328867933"></iframe>'

Page.create title: 'О нас', href: 'about', enabled: true, body: '
					<h1>О нас</h1>
					<p>
						Наше охранное предприятие занимается охраной жизни и имущества с 20хх года. За это время мы приобрели большой опыт, а так же профессионализм во многих видах охранных услуг.
					</p>
					<p>
						Высокое качество, а так же индивидуальный подход позволяют нам успешно сотрудничать со многими организациями, такими как:.........
					</p>
					<p>
						Обращаясь к нам Вы можете быть уверенными в том что получите компетентную помощь квалифицированных специалистов своего дела, которые профессионально подходят к каждой поставленной задаче и
						выполняют ее на высоком уровне.
					</p>
					<p>
						Если вы хотите ознакомиться с перечнем услуг, которые предоставляет наше охранное предприятие, то перейдите в раздел <a class="nav-href" href="#services">"Услуги"</a>. Если вы хотите связаться с нами, то перейдите в раздел <a href="#contact" class="nav-href">"Контакты"</a>
					</p>'

Service.create title: 'Пультовая охрана ресторанов, магазинов, банков, офисов', enabled: true, order: 1, desc: '
					<p>
						Пультовая охрана объектов представляет собой универсальный вид мероприятий по обеспечению защиты путем работы систем видеонаблюдения, пожарной сигнализации, а так же деятельности группы быстрого реагирования. цена такого вида услуг весьма невелика, что является положительным фактором. В наше время к такому типу услуг прибегают не только владельцы торговых центров, предприятий, но и владельцы частных домов и квартир.
					</p>
					<p>
						С помощью надежных современных технических средств пультовая охрана полностью исключает человеческий фактор, который может повлиять на качество услуг. Кроме технического оборудования так же круглосуточно работает служба наблюдения которая принимает поступающие сигналы и обеспечивает непрерывный мониторинг охраняемых объектов, а так же группа быстрого реагирования, готовая прибыть с первым сигналом тревоги в минимальное время.
					</p>
					<p>
						Наше охранное предприятие предлагает Вам большой выбор технических средств защиты, к примеру сотовую сигнализацию, которая может взять на себя функцию сигнализирования охраны, а так же управление находящимся на охраняемом объекте оборудованием. Новейшие технологии способны предоставить широчайшие возможности для охраны различных объектов.
					</p>'

Service.create title: 'Физическая охрана', enabled: true, order: 2, desc: '
					<p>
						Физическая охрана является основной профессиональной услугой предоставляемой нашим охранным предприятием. Целью данной услуги является охрана заводов, строительных площадок, фабрик, магазинов, банков, офисов, ночных клубов, торговых комплексов, а так же многих других объектов.
					</p>
					<p>
						Кроме охраны объектов мы готовы обеспечить безопасность различных мероприятий: выставок, презентаций, концертов и т.д. К каждому конкретному мероприятию и объекту мы подходим индивидуально, разрабатывая эффективную схему по обеспечению охраны. Этому способствует предварительная экспертиза охраняемого объекта. Охрана подбирается с учетом пожеланий заказчика.
					</p>
					<ul>
						Услуга физической охраны включает в себя:
						<li>- выставление постов охраны на охраняемом объекте</li>
						<li>- патрулирование территории на охраняемом объекте</li>
						<li>- индивидуальный подход к каждому охраняемому объекту</li>
						<li>- обеспечение контрольно-пропускного режима на объекте</li>
					</ul>'

Service.create title: 'Охрана квартир', enabled: true, order: 3, desc: '
					<p>
						Наше предприятие оказывает профессиональные услуги по охране квартир, которые включают в себя установку, подключение и обслуживание охранных и охранно-пожарных сигнализаций. Мы предлагаем вам современные охранные сигнализации, которые позволят защитить Ваше жилище от проникновений злоумышленников, протечек воды, пожаров и других неприятностей.
					</p>
					<p>
						Если Вы закажете эту услугу у нас, то Вы всегда будете уверены в том, что ваша квартира находится в безопасности.
					</p>
					<p>
						Круглосуточный мониторинг систем сигнализаций позволяет нам быстро реагировать на первые сигналы о тревоге, что, в свою очередь помогает нам обезопасить Ваше имущество.
					</p>'

Service.create title: 'Сопровождение грузов (по России)', enabled: true, order: 4, desc: '
					<p>
						Сопровождение грузов - вид услуг нашего охранного предприятия, целью, которой является охрана материальных ценностей заказчика. Наше предприятие охраняет груз на всем пути его следования по территории РФ.
					</p>
					<p>
						Сопровождение грузов осуществляется квалифицированными специалистами, которые имеют большой опыт в подобных мероприятиях.
					</p>
					<p>
						Наше охранное предприятие может предоставить вам как вооруженное, так и невооруженное сопровождение груза, все зависит от конкретно поставленной задачи, к каждому конкретному случаю мы подходим индивидуально.
					</p>
					<p>
						Наше предприятие несет ответственность за перевозимый груз. Большой опыт работы, оснащение, профессионализм позволяют нам обеспечить высокую безопасность Вашего имущества.
					</p>'

Service.create title: 'Проектирование и монтаж систем безопасности', enabled: true, order: 5, desc: '
					<p>
						Мы сотрудничаем как с физическими, так и с юридическими лицами, если вы хотите установить пожарную сигнализацию, видеонаблюдение, тревожную кнопку, то оставьте заявку на электронную почту либо свяжитесь с нами по телефону
					</p>'
class CreatePages < ActiveRecord::Migration
  def change
    create_table :pages do |t|
      t.string :title
      t.string :href
      t.text :body
      t.string :meta_title
      t.string :meta_desc
      t.string :meta_keywords
      t.boolean :enabled

      t.timestamps null: false
    end
  end
end

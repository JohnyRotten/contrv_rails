class CreateMenus < ActiveRecord::Migration
  def change
    create_table :menus do |t|
      t.string :title
      t.string :href
      t.boolean :enabled
      t.integer :order

      t.timestamps null: false
    end
  end
end

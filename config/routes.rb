Rails.application.routes.draw do
	root 'welcome#home'

	resources :users
	resources :services
	resources :menus
	resources :pages
	resources :sessions,	only: [ :new, :create, :destroy ]

	match '/signin',	to: 'sessions#new',		via: 'get'
	match '/signout',	to: 'sessions#destroy',	via: 'delete'
	match '/signup',	to: 'users#new',		via: 'get'
end
